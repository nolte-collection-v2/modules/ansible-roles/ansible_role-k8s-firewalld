# K8S FirewallD Role Configuration

Wrapper for the Ansible Role [FlatKey/ansible-firewalld-role](https://github.com/FlatKey/ansible-firewalld-role). Using for generate FirewallD roles, based on Ansible Inventory Groups.

## Usage

```yaml
```

## Testing

Using a Combination of Vagrant and Molecule for execute the sources in a separated environment.

```sh
molecule test
```






